﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CodeRunner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            string content = this.inputBox.Text;

            if(string.IsNullOrEmpty(content))
            {
                MessageBox.Show("Content has no value!");
            }
            else
            {
                // Get the current List items
                this.listBox.Items.Add(content);
            }

        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            this.listBox.Items.Clear();
        }

        private void listBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Console.WriteLine("Listbox size has changed!");
            int items = this.listBox.Items.Count;
            if(items == 0)
            {
                this.clearButton.Content = "Clear";
            }
            else
            {
                String buttonText = String.Format("Clear ({0})", this.listBox.Items.Count);
                this.clearButton.Content = buttonText;
            }
        }


    }
}
